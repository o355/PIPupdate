# PIPupdate-nocolor Version 1.3.1
# Copyright (C) 2016-2019  o355
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

print("Welcome to PIPupdate-nocolor (v1.3.1)!")
print("Loading...")

updatecountint = 0
updatenumber = 0

try:
    import sys
except ImportError:
    raise ImportError("Please install sys to use PIPupdate-nocolor!")

try:
    import pkg_resources
except ImportError:
    raise ImportError("Please install pkg_resources to use PIPupdate!")

if sys.version_info[0] < 3:
    print("Please use Python 3.0 or greater to use PIPupdate-nocolor!")
    sys.exit()

# In the color script, the PIP install options come up before the Colorama install.
# There's a little except and exit here for proper handling.
# In the event someone just runs the NC script off the bat.
try:
    from pip._internal import main
except ImportError:
    print("Shucks. PIP isn't installed. Would you like me to install PIP for you?")
    pipinstall = input("Yes or No: ").lower()
    if pipinstall == "yes":
        print("Alright. Installing PIP now!")
        exec(open("pipinstall.py").read())
    elif pipinstall == "no":
        print("Alright. Not installing PIP, exiting PIPupdate.")
        sys.exit()
    else:
        print("I couldn't understand what you inputted.")
        print("I'll assume you didn't want to install PIP, exiting now.")
        sys.exit()

installed_pkgs = pkg_resources.working_set
for i in installed_pkgs:
    updatenumber = updatenumber + 1

updatenumberstr = str(updatenumber)

print("Would you like to update %s pip packages? Enter Yes or No (Y/N also works)" % updatenumberstr)
confirmation_input = input("Input here: ").lower()
if confirmation_input != "yes" and confirmation_input != "y":
    if confirmation_input != "no" and confirmation_input != "n":
        print("Your PIP packages will not be updated, as your input was not understood.")
        sys.exit()
    else:
        print("Your PIP packages will not be updated.")
        sys.exit()

for i in installed_pkgs:
    pkgname = i.key
    updatecountint = updatecountint + 1
    updatecountstr = str(updatecountint)
    print("Now updating package " + pkgname + " (" + updatecountstr + "/" + updatenumberstr + ")...")
    print("- - - - - - - - - - - - - - - - - - - - - -")
    try:
        main(['install', '--upgrade', pkgname])
        print("- - - - - - - - - - - - - - - - - - - - - -")
        print("Updated package " + pkgname + ".")
    except:
        print("- - - - - - - - - - - - - - - - - - - - - -")
        print("Failed to update package " + pkgname + ".")

print("")
try:
    print("- - - - - - - - - - - - - - - - - - - - - -")
    print("PIPupdate is done, updated " + updatecountstr + " packages!")
except:
    print("- - - - - - - - - - - - - - - - - - - - - -")
    print("PIPupdate is done, updated 0 packages!")
print("Thank you for using PIPupdate!")

