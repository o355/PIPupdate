# PIPupdate Changelog

**Latest version for pip 10 or higher: v1.3.1**

**Latest version for pip 9 or lower: v1.1.0**

# Version 1.3.1 for pip 10 or higher (released September 3, 2019)
* Fixed a critical bug where input processing was not done correctly.

# Version 1.3.0 for pip 10 or higher (released September 3, 2019)
* Added confirmation to update packages
* Better UI for separating PIP messages from PIPupdate output
* Better progress indication

# Version 1.2.0 for pip 10 or higher (released September 3, 2019)
* PIPupdate now works with PIP 10 or higher. Testing is underway so 1.2.0 may not fully work!
* Minor UI changes
* Updated copyright

# Version 1.1.0 for pip 9 or lower (released July 26, 2017)
* Added a catch when an update fails
* No longer requires subprocess (using pip.main to execute update commands)
* PIPinstall can catch a failed download, and allows the user to manually download the file instead.
* Minor UI changes
* Updated copyright

# Version 1.0.2 (released January 11, 2017)
* Fixes a bug in which if a sys import failed in the install script, the program would not exit properly.
* Small setup.txt modifications for the ZIPs.
* Quick modifications to the changelog.

# Version 1.0.1 (released December 31, 2016)
* Introduces handling of non-Python 3 runs (across all scripts).
* Introduces a no-color version of PIPupdate for those not wanting to install Colorama.
* Spaced out the code so it looks better, and removed most comments for now.
* Redid the setup.txt file in the provided ZIPs.
* Other small formatting/style changes.

# Version 1.0 (released December 27, 2016)
* Adds a script that lets users without PIP install PIP.
* Modifies setup.txt in the provided ZIPs.
* Let there be colors! Adds colors to distinguish PIPupdate output from PIP output (via Colorama).
* From the above line, also adds the ability to install Colorama from the program.

# Version 0.9.1 (released December 26, 2016)
* Adds comments.
* Adds proper ImportError handling of all imports.

# Version 0.9 (released December 26, 2016)
* Initial release.
