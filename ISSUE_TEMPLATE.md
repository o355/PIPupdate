Thanks for your support of PIPupdate, and your kindness of reporting an issue. Before you report an issue, take note of these few things:
* If you already know what's wrong with the code (Python gives a reasonable amount of information from the interpreter), slap it into an issue, and I'll make stuff happen.
* If you don't know what's going on, post the error code, and slap it into an issue.
* Don't post things like: "How do I install PIP? Why is this program asking me to install PIP?"
* Don't post an issue when you fall into a PIPupdate failsafe. Those things exist for good reason.
* Common errors are in the Wiki page about them. Search your issue there, or in other issues. If your issue doesn't exist in either places, post the issue.

And...don't forget some information!
* What OS you're running
* What Python version you have (simply open up Python and it'll tell you). If your version number is below 3, try running PIPupdate with Python 3.
* The EXACT error information. Copy and paste it.

Thanks for your contributions to PIPupdate, and being a cool person.
